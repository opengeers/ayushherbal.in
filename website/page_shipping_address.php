<?
include 'checklogin.php';
?>
<div class="alert alert-dismissible alert-info">
    <strong>Welcome to Ayush herbal!</strong> Which is a trusted and reliable online shopping store.
</div>
<div class="row">
    <div class="col-sm-3">
        <?php include 'my_account_left_panel.php'; ?>
    </div>

    <div class="col-sm-9">
        <div class="page-header">
            <h1>Shipping Address<small></small></h1>
        </div>
        <form method="POST" class="form-horizontal" action="<?= URL_BASE ?>process.php">
            <input type="hidden" name="action" value="shipping_update"> 
            
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" name="name" class="form-control">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="text" name="email" class="form-control">
                </div>
                <div class="form-group">
                    <label>Phone</label>
                    <input type="text" name="phone" class="form-control">
                </div>
                <div class="form-group">
                    <label>Address line 1</label>
                    <input type="text" name="addressline1" class="form-control">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Address line 2</label>
                    <input type="text" name="addressline1" class="form-control">
                </div>
                <div class="form-group">
                    <label>State</label>
                    <input type="text" name="state" class="form-control">
                </div>
                <div class="form-group">
                    <label>City</label>
                    <input type="text" name="city" class="form-control">
                </div>
                <div class="form-group">
                    <label>Pin Code</label>
                    <input type="text" name="pincode" class="form-control">
                </div>


            </div>
            <button type="submit" class="btn btn-success">Submit</button>
            
            </form>
    </div>
</div>