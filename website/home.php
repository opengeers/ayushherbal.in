<?php
// Call APIs
$res_cat_list = json_decode($fn->callcurl('POST', 'category_list', '{"parentid":"","status":"Publish"}'), TRUE);
$res_featured_list = json_decode($fn->callcurl('POST', 'product', '{"category": "","limit": "10"}'), TRUE);
$res_featured_list_data = $res_featured_list['data'];
?>
<div class="alert alert-dismissible alert-info">
    <strong>Welcome to Ayush herbal!</strong> Which is a trusted and reliable online shopping store.
</div>
<div class="row">
    <div class="col-sm-3">
        <?php include 'leftpanel.php'; ?>    
    </div>
    <link href="<?php echo URL_BASE ?>library/owl.carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
    <div class="col-sm-9">
        <div class="page-header">
            <h1>Featured Product<small></small></h1>
        </div>
        <!-- Owl slider-->            
        <div id="demo">
            <div id="owl-demo" class="owl-carousel">
                <?php foreach ($res_featured_list_data as $key => $value) { ?>
                    <div class="col-sm-12 items">
                        <a href="<?php echo URL_BASE.$res_featured_list_data[$key]['permalink']; ?>">
                        <div class="thumbnail">
                            <img src="http://ayushherbal.in/og-content/uploads/thumb/<? echo $res_featured_list_data[$key]['picture']; ?>" alt="...">
                            <div class="caption">
                                <p class="title"><? echo $res_featured_list_data[$key]['name']; ?></p>
                                <p class="pric">$ <? echo $res_featured_list_data[$key]['price']; ?></p>
                            </div>
                        </div>
                        </a> 
                    </div>                    
                <?php } ?>
                <script src="<?php echo URL_BASE ?>library/owl.carousel/owl-carousel/owl.carousel.js"></script>
            </div>
        </div>

        <div class="page-header">
            <h1>Top Selling Product<small></small></h1>
        </div>
        <!-- Owl slider-->            
        <div id="demo">
            <div id="owl-demo1" class="owl-carousel">
                <?php foreach ($res_featured_list_data as $key => $value) { ?>
                    <div class="col-sm-12 items">
                        <a href="<?php echo URL_BASE.$res_featured_list_data[$key]['permalink']; ?>">
                        <div class="thumbnail">
                            <img src="http://ayushherbal.in/og-content/uploads/thumb/<? echo $res_featured_list_data[$key]['picture']; ?>" alt="...">
                            <div class="caption">
                                <p class="title"><? echo $res_featured_list_data[$key]['name']; ?></p>
                                <p class="pric">$ <? echo $res_featured_list_data[$key]['price']; ?></p>
                            </div>
                        </div>
                        </a>    
                    </div>                    
                <?php } ?>
                <script src="<?php echo URL_BASE ?>library/owl.carousel/owl-carousel/owl.carousel.js"></script>
            </div>
        </div>
        
        <div class="page-header">
            <h1>latest Product<small></small></h1>
        </div>
        <!-- Owl slider-->            
        <div id="demo">
            <div id="owl-demo1" class="owl-carousel">
                <?php foreach ($res_featured_list_data as $key => $value) { ?>
                    <div class="col-sm-12 items">
                        <a href="<?php echo URL_BASE.$res_featured_list_data[$key]['permalink']; ?>">
                        <div class="thumbnail">
                            <img src="http://ayushherbal.in/og-content/uploads/thumb/<? echo $res_featured_list_data[$key]['picture']; ?>" alt="...">
                            <div class="caption">
                                <p class="title"><? echo $res_featured_list_data[$key]['name']; ?></p>
                                <p class="pric">$ <? echo $res_featured_list_data[$key]['price']; ?></p>
                            </div>
                        </div>
                        </a>    
                    </div>                    
                <?php } ?>
                <script src="<?php echo URL_BASE ?>library/owl.carousel/owl-carousel/owl.carousel.js"></script>
            </div>
        </div>
    </div>
</div>