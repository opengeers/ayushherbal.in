<?php 
    $res_cat_list = json_decode($fn->callcurl('POST', 'category_list', '{"parentid":"","status":"Publish"}'), TRUE);
?>
<footer>
    <div class="container">
        <div class="row footertop">
            <div class="col-sm-9">
                <h4>Ayush herbal</h4>   
                <div class="footer03">
            AyushHerbal.com is India's largest online health & fitness store for men and women. Shop online from the latest collections of health, fitness and similar products featuring the best brands.
        </div>
        <div class="footer04">
            <b>Disclaimer:</b> The information contained on AyushHerbal (www.ayushherbal.in or subdomains) is provided for informational purposes only and is not meant to substitute for the advice provided by your doctor or other healthcare professional. Information and statements regarding products, supplements, programs etc listed on AyushHerbal have not been evaluated by the Food and Drug Administration or any government authority and are not intended to diagnose, treat, cure, or prevent any disease. Please read product packaging carefully prior to purchase and use. The results from the products will vary from person to person. No individual result should be seen as typical.
        </div>
            </div>
            <div class="col-sm-3">
                <h4>Connect with us</h4>   
                <p>Find us on <span class="label label-primary">Facebook</span></p>
                <p>Follow us on <span class="label label-primary">Twitter</span></p>
                <p>Watch on <span class="label label-warning">Youtube</span></p>
            </div>
        </div>
        
        <div class="footer05">
            <div class="row">
                <div class="col-sm-6">Copyright <i class="fa fa-copyright" aria-hidden="true"></i> 2015-2016, Ayushherbal.in</div>
                <div class="col-sm-6 text-right">Designed & developed by <a href="http://www.opengeers.com" target="_blank">Opengeers Technology</a></div>
            </div>
        </div>    
    </div>
</footer>
<script src="<?php echo URL_BASE ?>js/custom.js" type="text/javascript"></script>