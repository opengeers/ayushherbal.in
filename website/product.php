<?php
$res_cat_list = json_decode($fn->callcurl('POST', 'category_list', '{"parentid":"","status":"Publish"}'), TRUE);
$prodid = $pagetype['id'];
$prod_detail = json_decode($fn->callcurl('GET', 'product_details/' . $prodid . '', ''), TRUE);
$prod_detail_data = $prod_detail['data'];
$prod_detail_data_meta = $prod_detail['post_meta'];
?>
<div class="alert alert-dismissible alert-info">
    <strong>Welcome to Ayush herbal!</strong> <?php echo $prod_detail_data['name'] ?>
</div>

<div class="row">
    <div class="col-sm-3">
        <?php include 'leftpanel.php'; ?>    
    </div>

    <div class="col-sm-9">
        <div class="row">
            <div class="col-sm-4 prodetimage">
                <img src="http://ayushherbal.in/og-content/uploads/thumb/<? echo $prod_detail_data['picture']; ?>" alt="...">
            </div>    
            <div class="col-sm-8">
                <h3><? echo $prod_detail_data['name']; ?></h3>
                <p><? echo $prod_detail_data['content']; ?></p>
                <p class="rprice" >Retail Price : <span>$<? echo $prod_detail_data['retail_price']; ?></span></p>
                <p class="oprice">Our Price : <span>$<? echo $prod_detail_data['price']; ?></span></p>
                <p>
                    <a href="<?php echo URL_BASE;?>process.php?action=add2cart&id=<? echo $prod_detail_data['id']; ?>" class="btn btn-success">Buy Now</a>
                </p>
            </div>
        </div> 
        
        <div class="panel panel-info">
  <div class="panel-heading">More Details</div>
  <div class="panel-body">
    <?php foreach ($prod_detail_data_meta as $key => $value) { ?>
                    <p class="postmeta">
                        <span><? echo str_replace('-', ' ', $prod_detail_data_meta[$key]['meta_key']); ?></span> : <? echo $prod_detail_data_meta[$key]['meta_value']; ?>
                    </p>
                <?php } ?>
  </div>
</div>
        
        
        <?php
        $res_featured_list = json_decode($fn->callcurl('POST', 'product', '{"category": "","limit": "4"}'), TRUE);
        $res_featured_list_data = $res_featured_list['data'];
        ?>
        <div class="relatedproduct">
            <div class="page-header">
                <h1>Related products<small></small></h1>
            </div>
            <?php
            foreach ($res_featured_list_data as $key => $value) {
                include 'postlistloop.php';
            }
            ?>
        </div>  
    </div>
</div>