<?
include 'checklogin.php';
?>
<div class="alert alert-dismissible alert-info">
    <strong>Welcome to Ayush herbal!</strong> Which is a trusted and reliable online shopping store.
</div>
<div class="row">
    <div class="col-sm-3">
        <?php include 'my_account_left_panel.php'; ?>
    </div>

    <div class="col-sm-9">
        <div class="page-header">
            <h1>Track Order<small></small></h1>
        </div>
        <form method="POST"  action="">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="input-group">
                    <input type="text" name="tracking_id" class="form-control" placeholder="Enter order tracking id..." value="<?=$_POST['tracking_id'];?>">
                    <span class="input-group-btn">
                        <button class="btn btn-success" type="submit">Go!</button>
                    </span>
                </div>
            </div>
        </div>
        </form>
<br><br>
        <?
        $data = array('tracking_id' => $_POST['tracking_id']);
        $data_json = json_encode($data);
        $res_cart = json_decode($fn->callcurl('POST', 'track_order', $data_json), TRUE);
        if ($res_cart['status'] == 'success') {
            ?>
            <div class="table-responsive cart-div">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="w50 text-center">S.No.</th>
                            <th>Perticulars</th>
                            <th> Code</th>
                            <th class="w75 text-center">Qty.</th>
                            <th class="w100 text-right">Unit Price</th>
                            <th class="w100 text-right">Total Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $order_details = $res_cart['order_details'];
                        foreach ($order_details as $key => $value) {
                            ?>
                            <tr>
                                <td class="text-center"><?php echo $key + 1; ?></td>
                                <td><?php echo $order_details[$key]['product_name']; ?></td>
                                <td><?php echo $order_details[$key]['stock_number']; ?></td>
                                <td class="text-center"><?= $order_details[$key]['product_qty']; ?></td>
                                <td class="text-right">$<?php echo round($order_details[$key]['offer_price'], 2); ?></td>
                                <td class="text-right">$<?php echo round($order_details[$key]['totalprice'], 2); ?></td>
                            </tr>
                        <?php } ?>

                        <tr class="cart-footer">
                            <td colspan="2" >
                                <div class="panel panel-default panelcustom">
                                    <!-- Default panel contents -->
                                    <div class="panel-heading">Shipping Address</div>
                                    <div class="panel-body">
                                        <? $shipping_add = unserialize($res_cart['order']['shipping_info']); ?>
                                        <p><?= $shipping_add['shipping_name'] ?><br>
                                        <?= $shipping_add['shipping_add01'] ?> <?= $shipping_add['shipping_add02'] ?><br>
                                        <?= $shipping_add['shipping_city'] ?><br>
                                        <?= $shipping_add['shipping_state'] ?> - <?= $shipping_add['pin_code'] ?><br>
                                        Mo : <?= $shipping_add['shipping_phone'] ?><br>
                                        Email : <?= $shipping_add['shipping_email'] ?></p>
                                    </div>
                                </div>                                    

                            </td>
                            <td colspan="5" class="vm" >
                                <div class="cart-total text-center ">
                                    <strong>   Total Billing Amout : 

                                        <?= $res_cart['grandtotal']['grandtotal'] + $res_cart['order']['shipping_price'] ?> USD </strong> <br />
                                    <small class="text-muted">Including shipping charge  - <?= $res_cart['order']['shipping_price'] ?> USD</small>

                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        <? } else { ?>
            <div class="nodata">No order found.</div>
        <? } ?>
    </div>
</div>