<?php
$catid = $pagetype['id'];
?>
<?php
// Call APIs
$res_cat_list = json_decode($fn->callcurl('POST', 'category_list', '{"parentid":"","status":"Publish"}'), TRUE);
$res_cart = json_decode($fn->callcurl('POST', 'cart_page', '{"sessionid":"' . SESSION_ID . '"}'), TRUE);
$shipping_method = json_decode($fn->callcurl('GET', 'shipping_method', ''), TRUE);
$shipping_method_data = $shipping_method['data'];

//echo '<pre>';print_r($shipping_method_data);echo '</pre>';
?>
<div class="alert alert-dismissible alert-warning">
    <strong>Welcome to Ayush herbal!</strong> Which is a trusted and reliable online shopping store.
</div>
<div class="row">
    <div class="col-sm-3">
        <?php include 'leftpanel.php'; ?>   
    </div>

    <div class="col-sm-9">
        <div class="page-header">
            <h1>Preview order<small></small></h1>
        </div>
        <!-- Product list -->  
        <div class="row carttable">
            <div class="col-md-12">
                <div class="table-responsive cart-div">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="w50 text-center">S.No.</th>
                                <th>Perticulars</th>
                                <th> Code</th>
                                <th class="w75 text-center">Qty.</th>
                                <th class="w100">Unit Price</th>
                                <th class="w100">Total Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $order_details = $res_cart['order_details'];
                            foreach ($order_details as $key => $value) {
                                ?>
                                <tr>
                                    <td class="text-center"><?php echo $key + 1; ?></td>
                                    <td><?php echo $order_details[$key]['product_name']; ?></td>
                                    <td><?php echo $order_details[$key]['stock_number']; ?></td>
                                    <td class=" text-center"><?= $order_details[$key]['product_qty']; ?></td>
                                    <td>$ <?php echo round($order_details[$key]['offer_price'], 2); ?></td>
                                    <td>$ <?php echo round($order_details[$key]['totalprice'], 2); ?></td>
                                </tr>
                            <?php } ?>

                            <tr class="cart-footer">
                                <td colspan="2" >
                                    <div class="panel panel-default panelcustom">
                                        <!-- Default panel contents -->
                                        <div class="panel-heading">Shipping Method</div>
                                        <div class="panel-body">
                                            Please select your shipping method that  which you want to use for this order.
                                        </div>
                                        <!-- List group -->
                                        <ul class="list-group">
                                            <?php foreach ($shipping_method_data as $key => $value) { ?>
                                                <li class="list-group-item">
                                                    <input type="radio" name="shipping_method" 
                                                    <?
                                                    if ($res_cart['order']['shipping_method'] == $shipping_method_data[$key]['id']) {
                                                        echo 'checked';
                                                    } elseif ($key == 0) {
                                                        echo 'checked';
                                                    }
                                                    ?>
                                                           disabled="disabled" value="<?= $shipping_method_data[$key]['id'] ?>"> <?= $shipping_method_data[$key]['name'] ?><span class="badge">$<?= $shipping_method_data[$key]['value'] ?></span>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>                                    

                                </td>
                                <td colspan="4" class="vm" >
                                    <div class="cart-total text-center">
                                        <strong>   Total Billing Amout : 6500 USD </strong> <br />
                                        <small class="text-muted">all amount inclusive of 15% tax.</small>
                                        <br><br>
                                        <a href="<?= URL_BASE . 'shopping_cart' ?>" class="btn btn-info">Update order</a>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!--CART DIV END-->
            </div>
        </div>

        <form name="paynow" action="<?= URL_BASE ?>process.php" method="POST">
            <input type="hidden" name="action" value="payment_process">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Shipping information</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Name</label>
                                        <input class="form-control" id="focusedInput" type="text" name="shipping_name" value="">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Email</label>
                                        <input class="form-control" id="focusedInput" type="text" name="shipping_email" value="">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Address line 1</label>
                                        <input class="form-control" id="focusedInput" type="text" name="shipping_add01" value="">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Address line 2</label>
                                        <input class="form-control" id="focusedInput" type="text" name="shipping_add02" value="">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">State</label>
                                        <input class="form-control" id="focusedInput" type="text" name="shipping_state" value="">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">City</label>
                                        <input class="form-control" id="focusedInput" type="text" name="shipping_city" value="">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Phone</label>
                                        <input class="form-control" id="focusedInput" type="text" name="shipping_phone" value="">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Pin code</label>
                                        <input class="form-control" id="focusedInput" type="text" name="pin_code" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <button type="submit" class="btn btn-success">Pay Now</button>
                </div>
            </div>
        </form>
    </div>
</div>