<?php
$total_page = floor($res_featured_list['total_product'] / $prod_per_page);
$main_url = explode('page=', $fn->url_full());
$main_url = trim($main_url[0],'&');

if(strpos($fn->url_full(), '?')){
    $pageurl = '&page=';
}else{
    $main_url = $main_url.'?';
    $pageurl = 'page=';
}
        
if ($page > 0) {
    $prevlink = $pageurl.($page - 1) . '';
}
if ($page < $total_page) {
    $nexlink = $pageurl.($page + 1) . '';
}
?>
<div class="clear">
    <ul class="pager">
<? if ($page > 0) { ?><li><a href="<?= $main_url;?><?= $prevlink ?>">&larr; Previous </a></li><? } ?>
<? if ($page < $total_page) { ?><li><a href="<?= $main_url;?><?= $nexlink ?>">Next &rarr;</a></li><? } ?>
    </ul>
</div>