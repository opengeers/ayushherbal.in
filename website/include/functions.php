<?php

class functions {

    public function ip() {
        return ($_SERVER['REMOTE_ADDR']);
    }

    public function dat() {
        return (date("d M Y"));
    }

    public function datefull() {
        return (date("Y-m-d h:i:s"));
    }

    public function diff_min($date_db) {
        $from_time = strtotime(date($date_db));
        $to_time = strtotime(date("Y-m-d h:i:s"));
        return(round(abs($to_time - $from_time) / 60, 0));
    }

    public function redirect($url) {
        header('Location:' . $url . '');
        exit;
    }

    public function escapeString($data) {
        return mysql_real_escape_string($data);
    }

    public function md5password($data) {
        return md5($data);
    }

    public function url() {
        $root = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = end(explode('/', $root));
        $url = explode('?', $url);
        return $url[0];
    }
    
    public function url_full() {
        $root = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        return $root;
    }

    public function refurl() {
        $root = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        return $root;
    }

    public function encoded($str) {
        return base64_encode(base64_encode($str));
    }

    public function decoded($str) {
        return base64_decode(base64_decode($str));
    }

    public function callcurl($method, $url, $data) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => URL_API . $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }

}

$fn = new functions();
