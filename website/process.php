<?php
include 'include/config.php';
include 'include/functions.php';
$fn = new functions();
$action = $_REQUEST['action'];
switch ($action) {
    case 'add2cart':
        $prodid = $_REQUEST['id'];
        $result = json_decode($fn->callcurl('POST', 'add2cart', '{"id": "' . $prodid . '","sessionid": "' . SESSION_ID . '"}'), TRUE);
        if ($result['status'] != '0') {
            $fn->redirect(URL_BASE . 'shopping_cart');
        } else {
            $fn->refurl();
        }
        break;

    case 'update_cart':
        $data = $_POST;
        $data['sessionid'] = SESSION_ID;
        $data = json_encode($data, TRUE);
        $result = json_decode($fn->callcurl('POST', 'update_cart', $data), TRUE);
        if ($result['status'] != '0') {
            $fn->redirect(URL_BASE . 'preview_cart');
        } else {
            $fn->redirect(URL_BASE . 'shopping_cart');
        }
        break;

    case 'remore_product':
        $data['odid'] = $_REQUEST['odid'];
        $data = json_encode($data, TRUE);
        $result = json_decode($fn->callcurl('POST', 'remove_product_from_order', $data), TRUE);
        $fn->redirect(URL_BASE . 'shopping_cart');
        break;

    case 'payment_process':
        // Update shipping info
        $data = $_POST;
        $data['sessionid'] = SESSION_ID;
        $data = json_encode($data, TRUE);
        $result = json_decode($fn->callcurl('POST', 'update_shipping', $data), TRUE); // $data should be json format 
        if ($result['status'] != '0') {
            //success
            // get order info
            $res_cart = json_decode($fn->callcurl('POST', 'cart_page', '{"sessionid":"' . SESSION_ID . '"}'), TRUE);
            $order_details = $res_cart['order_details']; 
//            echo '<pre>';
//            print_r($res_cart);
            // Call Paypal 
            $paypal_url = 'https://www.paypal.com/cgi-bin/webscr';
            $paypal_email = 'contact@rockrosetrading.com';
            $paypal_retutn_url = URL_BASE.'preview_cart';
            $paypal_currency = 'USD';
            $_SESSION['paypal'] = '';
            $_SESSION['paypal'] = 1;
            ?>
            <form action="<?=$paypal_url?>" id="payment_paypal_form" name="payment_paypal_form" method="post">
                <input type="hidden" name="cmd" value="_cart">
                <input type="hidden" name="upload" value="1">
                <input type="hidden" name="business" value="<?=$paypal_email?>">
                <input type="hidden" name="cancel_return" id="cancel_return" value="<?= $paypal_retutn_url ?>" />
                <input type="hidden" name="return" value="<?= $paypal_retutn_url ?>" />
                <input type="hidden" name="currency_code" value="<?=$paypal_currency?>">
                <!-- Loop product-->
                <?php $i=0;foreach ($order_details as $key => $value) { $i++;?>
                    <input type="hidden" name="item_name_<?= $i ?>" value="<?=$order_details[$key]['product_name']?>">
                    <input type="hidden" name="amount_<?= $i ?>" value="<?=$order_details[$key]['offer_price']?>">
                    <input type="hidden" name="quantity_<?= $i ?>" value="<?=$order_details[$key]['product_qty']?>">
                <?php }?>
                <!-- Loop product end-->                    
                <input type="hidden" name="shipping_1" value="<?= $res_cart['order']['shipping_price']; ?>" />

            </form>
            <div class="laddn">Please wait... your payment is processing. Do not close or refresh.</div>
            <script>
                setTimeout(function () {
                    paypal_submit();
                }, 1000);
                function paypal_submit() {
                    document.payment_paypal_form.submit();
                }
            </script>
            <?php
        } else {
            // Error
            $fn->redirect(URL_BASE . 'preview_cart');
        }
        exit;


        break;

    case 'sign_up':
        $data = $_POST;        
        $data['role']='Subscriber';
        $data['status']= 'Publish';
        $data['type']='user';
        $data['date']= $fn->datefull();
        unset($data['action']);
       
        $result = json_decode($fn->callcurl('POST', 'sign_up', $data_json), TRUE);
        if ($result['status'] == 'success') {
            $fn->redirect(URL_BASE . 'login');
        } else {
            $fn->refurl();
        }
        break;
        
    case 'login':    
        $data = $_POST;        
        unset($data['action'],$data['remember']);
        $data_json = json_encode($data);
        $result = json_decode($fn->callcurl('POST', 'login', $data_json), TRUE);
        if ($result['status'] == 'success') {
            $_SESSION['loggedin']= array('token'=>$result['token'],'userinfo'=>$result['userinfo']);
            $fn->redirect(URL_BASE . 'my_account');
        } else {
            $fn->redirect(URL_BASE . 'login');
            //$fn->refurl();
        }
        
        break;
        
    case 'logout':    
        unset($_SESSION['loggedin']);
        $fn->redirect(URL_BASE . 'login');
        break;
    
    case 'update_password':    
        $data = $_POST;        
        unset($data['action']);
        $data_json = json_encode($data);
        $result = json_decode($fn->callcurl('POST', 'update_password', $data_json), TRUE);
        if ($result['status'] == 'success') {
            $_SESSION['loggedin']= array('token'=>$result['token'],'userinfo'=>$result['userinfo']);
            $fn->redirect(URL_BASE . 'my_account/change_password');
        } else {
            $fn->redirect(URL_BASE . 'my_account/change_password');
        }
        break;
    
    case 'shipping_update':    
        
        break;  
    
        
        
    default:
        break;
}