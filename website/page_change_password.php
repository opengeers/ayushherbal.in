<?
include 'checklogin.php';
?>
<div class="alert alert-dismissible alert-info">
    <strong>Welcome to Ayush herbal!</strong> Which is a trusted and reliable online shopping store.
</div>
<div class="row">
    <div class="col-sm-3">
        <?php include 'my_account_left_panel.php'; ?>
    </div>

    <div class="col-sm-9">
        <div class="page-header">
            <h1>Change Password<small></small></h1>
        </div>
        <div class="col-sm-12">
        <form method="POST" class="form-horizontal" action="<?= URL_BASE ?>process.php">
            <input type="hidden" name="action" value="update_password">    
            <div class="form-group">
                <label>Old Password</label>
                <input type="text" name="old_password" class="form-control w200">
            </div>
            <div class="form-group">
                <label>New Password</label>
                <input type="text" name="password" class="form-control w200">
            </div>
            <div class="form-group">
                <label>Confirm Password</label>
                <input type="text" name="confirm_password" class="form-control w200">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </form>
</div>
    </div>
</div>