<?php
$catid = $pagetype['id'];
?>
<?php
// Call APIs
$res_cat_list = json_decode($fn->callcurl('POST', 'category_list', '{"parentid":"","status":"Publish"}'), TRUE);
?>
<div class="alert alert-dismissible alert-success">
    <strong>Welcome to Ayush herbal!</strong> Which is a trusted and reliable online shopping store.
</div>
<div class="row">
    <div class="col-sm-3">
        <?php include 'leftpanel.php'; ?>    
    </div>

    <div class="col-sm-9">
        <div class="page-header">
            <h1>Products for - <?php echo $_REQUEST['q']; ?><small></small></h1>
        </div>
        <!-- Product list -->            
        <?php include 'include/pagination_top.php'; ?>
        <?php        
        if ($_REQUEST['q'] != '') {
            $data['q'] = $_REQUEST['q'];
        }
        $data_json = json_encode($data);
        $res_featured_list = json_decode($fn->callcurl('POST', 'product', $data_json), TRUE);
        if ($res_featured_list['status'] == 1) {
            $res_featured_list_data = $res_featured_list['data'];

            foreach ($res_featured_list_data as $key => $value) {
                include 'postlistloop.php';
            }
            ?>
            <?php include 'include/pagination_bottom.php'; ?>
        <? } else { ?>
            <div class="nodata">No product found.</div>
        <? } ?>
    </div>
</div>