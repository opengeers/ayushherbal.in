<div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">My Account</div>
    <div class="panel-body">
        <p>You can choose your category from the below category list to find your need easily.</p>
    </div>
    <!-- List group -->
    <ul class="list-group">
        <li><a class="list-group-item" href="<?=URL_BASE?>my_account/track_order">Track Order</a></li>
        <li><a class="list-group-item" href="<?=URL_BASE?>my_account/change_password">Change Password</a></li>
        <li><a class="list-group-item" href="<?=URL_BASE?>my_account/shipping_address">Shipping Address</a></li>
        <li><a class="list-group-item" href="<?=URL_BASE?>process.php?action=logout">logout</a></li>
    </ul>
</div>  