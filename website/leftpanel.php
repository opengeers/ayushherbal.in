<?php
$cart_sort_info = json_decode($fn->callcurl('POST', 'cart_sort_info', '{"sessionid":"'.SESSION_ID.'"}'), TRUE);
$cart_sort_info_data = $cart_sort_info['data'];
?>
<div class="panel panel-success">
    <!-- Default panel contents -->
    <div class="panel-heading">My shopping cart</div>
    <?php if($cart_sort_info_data['total_item_cart']>0){?>
    <div class="panel-body">
        <p>Below is the overall cart summary, So, you can check your order amount, total no of product and quantity always.
        <div class="text-center"><a href="<?=URL_BASE?>shopping_cart" class="btn btn-success btn-xs">Go to Cart</a></div>
        </p>
    </div>
    <!-- List group -->
    <ul class="list-group">
        <li><a class="list-group-item">Total items <span class="badge"><?php echo $cart_sort_info_data['total_item_cart']; ?></span></a></li>
        <li><a class="list-group-item">Total Quantity <span class="badge"><?php echo $cart_sort_info_data['quantity']; ?></span></a></li>
        <li><a class="list-group-item">Total Price <span class="badge">$<?php echo $cart_sort_info_data['total_price']; ?></span></a></li>
    </ul>
    <?php }else{?>
    <div class="panel-body text-center">
        <p>Cart is empty.</p>
        </div>
    <?php }?>
</div> 

<div class="panel panel-success">
    <!-- Default panel contents -->
    <div class="panel-heading">Our Category</div>
    <div class="panel-body">
        <p>You can choose your category from the below category list to find your need easily.</p>
    </div>
    <!-- List group -->
    <ul class="list-group">
        <?php foreach ($res_cat_list as $key => $value) { ?>
            <li><a class="list-group-item" href="<?php echo $res_cat_list[$key]['permalink']; ?>"><?php echo $res_cat_list[$key]['name']; ?> </a></li>
            <?php } ?>
    </ul>
</div> 