<?php
include 'include/config.php';
include 'include/functions.php';
?>
<?
//echo '<pre>';
//print_r($_SESSION['loggedin']);
//echo '</pre>';
?>
<!doctype html>
<html>
    <head>
        <title>Classified18.com</title>
<meta name="viewport" content="width=device-width, initial-scale=1" />
        <!--jQuery -->
        <script src="<?php echo URL_BASE; ?>library/jquery/jquery_3.1.0.min.js" type="text/javascript"></script>

        <!-- Bootstrap-->
        <link href="<?php echo URL_BASE; ?>library/bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="<?php echo URL_BASE; ?>library/bootstrap/bootstrap.min.js" type="text/javascript"></script>
        <!--font awesome -->
<!--        <link href="<?php echo URL_BASE; ?>library/fontawesome/font-awesome.min.css" rel="stylesheet" type="text/css"/>-->
<!--        <script src="https://use.fontawesome.com/64e5173dcf.js"></script>-->
        <!--Custom CSS -->
        <link href="<?php echo URL_BASE; ?>css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>

        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo URL_BASE; ?>"><img src="<?php echo URL_BASE; ?>images/logo.png" alt=""/></a>      
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">

                    </ul>
                    <form class="navbar-form navbar-left" action="<?=URL_BASE?>search" method="GET">
                        <div class="form-group">
                            <input type="text" class="form-control" name="q" value="<?=$_REQUEST['q']?>" placeholder="What are you looking for....">
                        </div>
                        <button type="submit" class="btn btn-success">Search</button>
                    </form>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?=URL_BASE?>why-us">Why Ayush?</a></li>
                        <li><a href="<?=URL_BASE?>about-us">About Us</a></li>
                        <li><a href="<?=URL_BASE?>contact-us">Contact Us</a></li>
                        <?php /*?>
                            <?php if($_SESSION['loggedin']['token'] || $_SESSION['loggedin']['token'] !=''){?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Account <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?=URL_BASE?>my_account/track_order">Track Order</a></li>
<!--                                <li><a href="<?=URL_BASE?>my_account/basic_profile">Basic Profile</a></li>-->
                                <li><a href="<?=URL_BASE?>my_account/change_password">Change Password</a></li>
                                <li><a href="<?=URL_BASE?>my_account/shipping_address">Shipping Address</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?=URL_BASE?>process.php?action=logout">Logout</a></li>
                            </ul>
                        </li>
                        <?php }else{?>
                            <li><a href="<?=URL_BASE?>login">My Account</a></li>
                        <?php }?>
                         * 
                         <?php */?>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>