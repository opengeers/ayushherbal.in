<?php include 'header.php'; ?>
<div class="container wrapper">
    <?php
    $current_page = $fn->url();
    $pagetype = json_decode($fn->callcurl('POST', 'getpageinfo', '{"inputfield": "permalink","inputval": "' . $current_page . '","outputfield": "type,id,name,content"}'), TRUE);
    if($current_page == ''){
        $page_load = 'home.php';
    }elseif($pagetype['type'] == 'page'){
        if($pagetype['content']!=''){
            $page_load = 'page.php';
        }else{
            $page_load = 'page_'.$current_page . '.php';
        }        
    }elseif($pagetype['type'] != ''){
        $page_load = $pagetype['type'] . '.php';
    }else {
        $page_load = 'page_'.$current_page . '.php';
    }
    include $page_load;
    ?>
</div>
<?php include 'footer.php'; ?>