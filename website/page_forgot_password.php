<?php
include 'loogedin.php';
// Call APIs
$res_cat_list = json_decode($fn->callcurl('POST', 'category_list', '{"parentid":"","status":"Publish"}'), TRUE);
$res_featured_list = json_decode($fn->callcurl('POST', 'product', '{"category": "","limit": "10"}'), TRUE);
$res_featured_list_data = $res_featured_list['data'];
?>
<!--<div class="alert alert-dismissible alert-info">
    <strong>Welcome to Ayush herbal!</strong> Which is a trusted and reliable online shopping store.
</div>-->

<div class="container">    
    <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
        <div class="panel panel-info" >
            <div class="panel-heading">
                <div class="panel-title">Forgot Password</div>
            </div>     

            <div style="padding-top:30px" class="panel-body" >

                <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

                <form id="loginform" class="form-horizontal" role="form">

                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="login-username" type="text" class="form-control" name="username" value="" placeholder="username or email">                                 </div>
                    
                    <div style="margin-top:10px" class="form-group">
                        <!-- Button -->

                        <div class="col-sm-12 controls">
                            <button class="btn btn-success">get password</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 control">
                            <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
                                Remember password 
                                <a class="btn btn-success btn-xs" href="<?=URL_BASE?>login">
                                    Sign In Here
                                </a>
                            </div>
                        </div>
                    </div>    
                </form>     
            </div>                     
        </div>  
    </div>

</div>

