<?php
include 'loogedin.php';
// Call APIs
$res_cat_list = json_decode($fn->callcurl('POST', 'category_list', '{"parentid":"","status":"Publish"}'), TRUE);
$res_featured_list = json_decode($fn->callcurl('POST', 'product', '{"category": "","limit": "10"}'), TRUE);
$res_featured_list_data = $res_featured_list['data'];
?>
<!--<div class="alert alert-dismissible alert-info">
    <strong>Welcome to Ayush herbal!</strong> Which is a trusted and reliable online shopping store.
</div>-->

<div class="container">    
    <div id="signupbox" style="margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">Sign Up</div>
            </div>  
            <div class="panel-body" >
                <form method="POST" id="signupform" class="form-horizontal" action="<?= URL_BASE ?>process.php">
                    <input type="hidden" name="action" value="sign_up">    

                    <div id="signupalert" style="display:none" class="alert alert-danger">
                        <p>Error:</p>
                        <span></span>
                    </div>

                    <div class="form-group">
                        <label for="email" class="col-md-3 control-label">Email</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="email">
                        </div>
                    </div>                                    
                    <div class="form-group">
                        <label for="firstname" class="col-md-3 control-label">First Name</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="first_name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lastname" class="col-md-3 control-label">Last Name</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="last_name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-md-3 control-label">Password</label>
                        <div class="col-md-9">
                            <input type="password" class="form-control" name="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="icode" class="col-md-3 control-label">Phone</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="phone">
                        </div>
                    </div>
                    <div class="form-group">
                        <!-- Button -->                                        
                        <div class="col-md-offset-3 col-md-9">
                            <button id="btn-signup" type="submit" class="btn btn-info"><i class="icon-hand-right"></i> &nbsp Sign Up</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 control">
                            <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
                                Already have account?
                                <a href="<?= URL_BASE ?>login">
                                    Sign In Here
                                </a>
                            </div>
                        </div>
                    </div> 

                </form>
            </div>
        </div>
    </div> 
</div>


