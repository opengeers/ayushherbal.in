<?php
error_reporting (E_ALL ^ E_NOTICE);
error_reporting(E_ERROR | E_PARSE);
// Slim config
require 'vendor/autoload.php';
$app = new Slim\App();

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

// Database credentials
$_SERVER['db_host'] = 'localhost';
$_SERVER['db_username'] = 'root';
$_SERVER['db_password'] = '';
$_SERVER['db_name'] = 'ayushherbal.in';

try {
    $conn = new PDO("mysql:host=" . $_SERVER['db_host'] . ";dbname=" . $_SERVER['db_name'] . "", $_SERVER['db_username'], $_SERVER['db_password']);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // echo "Connected successfully"; 
} catch (PDOException $e) {
    echo json_encode(array("err001" => "Database Connection not established/Network error"));
    die;
}
?>

