<?php

class functions {

    public function beginTransaction() {
        global $conn;
        $conn->beginTransaction();
    }

    public function commit() {
        global $conn;
        $conn->commit();
    }

    public function rollBack() {
        global $conn;
        $conn->rollBack();
    }

    public function select($table, $rows = '*', $join = null, $where = null, $order = null, $limit = null) {
        global $conn;
        $q = 'SELECT ' . $rows . ' FROM ' . $table;
        if ($join != null) {
            $q .= ' JOIN ' . $join;
        }
        if ($where != null) {
            $q .= ' WHERE ' . $where;
        }
        if ($order != null) {
            $q .= ' ORDER BY ' . $order;
        }
        if ($limit != null) {
            $q .= ' LIMIT ' . $limit;
        }
        //echo $q;
        $stmt = $conn->prepare($q);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        return($result);
    }

    public function sql($sql) {
        global $conn;
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        return($result);
    }

    public function executequery($sql) {
        global $conn;
        $stmt = $conn->prepare($sql);
        $return = $stmt->execute();
        if ($return) {
            return true;
        } else {
            return false;
        }
    }

    public function insert($table, $params = array()) {
        global $conn;
        $sql = "INSERT INTO $table (`" . implode("`, `", array_keys($params)) . "`) VALUES ('" . implode("', '", $params) . "')";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        if ($conn->lastInsertId()) {
            $lastinsertid = $conn->lastInsertId();
            return($lastinsertid);
        } else {
            return($conn->errorInfo());
        }
    }

    public function update($table, $params = array(), $where) {
        global $conn;
        $args = array();
        foreach ($params as $field => $value) {
            // Seperate each column out with it's corresponding value
            $args[] = $field . "='" . $value . "'";
        }
        // Create the query
        $sql = 'UPDATE ' . $table . ' SET ' . implode(',', $args) . ' WHERE ' . $where;
        $stmt = $conn->prepare($sql);
        if ($stmt->execute()) {
            return true;
        } else {
            return($conn->errorInfo());
        }
    }
    
    public function delete($table, $where) {
        global $conn;
        // Create the query
        $sql = 'DELETE FROM ' . $table . ' WHERE ' . $where;
        $stmt = $conn->prepare($sql);
        $stmt->execute();
    }

    public function ip() {
        return ($_SERVER['REMOTE_ADDR']);
    }

    public function dat() {
        return (date("d M Y"));
    }

    public function datefull() {
        return (date("Y-m-d h:i:s"));
    }

    public function diff_min($date_db) {
        $from_time = strtotime(date($date_db));
        $to_time = strtotime(date("Y-m-d h:i:s"));
        return(round(abs($to_time - $from_time) / 60, 0));
    }

    public function redirect($url) {
        header('Location:' . $url . '');
        exit;
    }

    public function escapeString($data) {
        return mysql_real_escape_string($data);
    }

    public function md5password($data) {
        return md5($data);
    }

    public function url() {
        $root = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = end(explode('/', $root));
        $url = explode('?', $url);
        return $url[0];
    }

    public function refurl() {
        $root = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        return $root;
    }

    public function encoded($str) {
        return base64_encode(base64_encode($str));
    }

    public function decoded($str) {
        return base64_decode(base64_decode($str));
    }

    public function logged_user($str) {
        return($_SESSION['loggedin']);
    }

    public function pagination($query, $per_page = 10, $page = 1, $url = '?') {
        global $conn;
        $sql = "SELECT COUNT(*) as `num` FROM {$query}";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_OBJ);
        $total = $result->num;
        $adjacents = "2";

        $prevlabel = "&lsaquo; Prev";
        $nextlabel = "Next &rsaquo;";
        $lastlabel = "Last &rsaquo;&rsaquo;";

        $page = ($page == 0 ? 1 : $page);
        $start = ($page - 1) * $per_page;

        $prev = $page - 1;
        $next = $page + 1;

        $lastpage = ceil($total / $per_page);

        $lpm1 = $lastpage - 1; // //last page minus 1

        $pagination = "";
        if ($lastpage > 1) {
            $pagination .= "<ul class='pagination'>";
            $pagination .= "<li class='page_info'>Page {$page} of {$lastpage}</li>";

            if ($page > 1)
                $pagination.= "<li><a href='{$url}page={$prev}'>{$prevlabel}</a></li>";

            if ($lastpage < 7 + ($adjacents * 2)) {
                for ($counter = 1; $counter <= $lastpage; $counter++) {
                    if ($counter == $page)
                        $pagination.= "<li><a class='current'>{$counter}</a></li>";
                    else
                        $pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";
                }
            } elseif ($lastpage > 5 + ($adjacents * 2)) {

                if ($page < 1 + ($adjacents * 2)) {

                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                        if ($counter == $page)
                            $pagination.= "<li><a class='current'>{$counter}</a></li>";
                        else
                            $pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";
                    }
                    $pagination.= "<li class='dot'>...</li>";
                    $pagination.= "<li><a href='{$url}page={$lpm1}'>{$lpm1}</a></li>";
                    $pagination.= "<li><a href='{$url}page={$lastpage}'>{$lastpage}</a></li>";
                } elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {

                    $pagination.= "<li><a href='{$url}page=1'>1</a></li>";
                    $pagination.= "<li><a href='{$url}page=2'>2</a></li>";
                    $pagination.= "<li class='dot'>...</li>";
                    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                        if ($counter == $page)
                            $pagination.= "<li><a class='current'>{$counter}</a></li>";
                        else
                            $pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";
                    }
                    $pagination.= "<li class='dot'>..</li>";
                    $pagination.= "<li><a href='{$url}page={$lpm1}'>{$lpm1}</a></li>";
                    $pagination.= "<li><a href='{$url}page={$lastpage}'>{$lastpage}</a></li>";
                } else {

                    $pagination.= "<li><a href='{$url}page=1'>1</a></li>";
                    $pagination.= "<li><a href='{$url}page=2'>2</a></li>";
                    $pagination.= "<li class='dot'>..</li>";
                    for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                        if ($counter == $page)
                            $pagination.= "<li><a class='current'>{$counter}</a></li>";
                        else
                            $pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";
                    }
                }
            }

            if ($page < $counter - 1) {
                $pagination.= "<li><a href='{$url}page={$next}'>{$nextlabel}</a></li>";
                $pagination.= "<li><a href='{$url}page=$lastpage'>{$lastlabel}</a></li>";
            }

            $pagination.= "</ul>";
        }

        return $pagination;
    }

    public function settingdata($field = '') {
        global $conn;
        if ($field != '') {
            $where = 'WHERE metakey= "' . $field . '" ';
        } else {
            $where = '';
        }
        $q = 'SELECT metakey,metavalue FROM ' . SETTING . ' ' . $where . ' ';
        $stmt = $conn->prepare($q);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        foreach ($result as $key => $val) {
            $result_setting[$result[$key]->metakey] = $result[$key]->metavalue;
        }
        return($result_setting);
    }

    public function curl($url, $data, $authorization,$method='POST') {         
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                "authorization: Token " . $authorization . "",
                "content-type: application/json"
            ),
        ));
        $response = curl_exec($curl);
        return $response;
    }
        
    
}

$fn = new functions();

function jsonoutput($data) {
    return $data.'';
}
?>