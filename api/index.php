<?php

error_reporting(E_ALL ^ E_NOTICE);
include "include/config.php";
include "include/functions.php";

// Define app routes
// POST Methodproduct
$app->post("/sign_up", function ($request, $response) {
    global $conn;
    global $fn; // database and functions

    $data = $request->getParsedBody();
    $input_array = json_decode($request->getBody(), true); // array
    $input_array['password'] = $fn->md5password($input_array['password']);

    $q = 'SELECT * FROM og_users WHERE email = "' . $input_array['email'] . '"';
    $stmt = $conn->prepare($q);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_OBJ);
    if ($result) {
        $jsonoutput = array("status" => "error", "msg" => "Email [" . $input_array['email'] . "] Duplicate, please use another email id. ");
    } else {
        $fn->insert('og_users', $input_array);
        $jsonoutput = array("status" => "success", "msg" => "Successfully registered [" . $input_array['email'] . "]");
    }
    // JSON output
    echo jsonoutput(json_encode($jsonoutput));
});

$app->post("/login", function ($request, $response) {
    global $conn;
    global $fn; // database and functions
    $data = $request->getParsedBody();
    $input_array = json_decode($request->getBody(), true); // array
    $password = $fn->md5password($input_array['password']);
    $token = $fn->md5password($input_array['email']);

    $q = 'SELECT * FROM og_users WHERE email = "' . $input_array['email'] . '" AND password = "' . $password . '" AND status="active" ';
    $stmt = $conn->prepare($q);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_OBJ);
    if ($result) {
        $jsonoutput = array(
            "status" => "success",
            "token" => $token,
            "userinfo" => $result,
            "msg" => "Login success."
        );
    } else {
        $jsonoutput = array("status" => "error", "msg" => "Invalid email or password.");
    }
    // JSON output
    echo jsonoutput(json_encode($jsonoutput));
});

$app->post("/forgot_password", function ($request, $response) {
    global $conn;
    global $fn; // database and functions
    $data = $request->getParsedBody();
    $input_array = json_decode($request->getBody(), true); // array    
    $q = 'SELECT * FROM og_users WHERE email = "' . $input_array['email'] . '"';
    $stmt = $conn->prepare($q);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_OBJ);
    if ($result) {
        $jsonoutput = array(
            "status" => "success",
            "msg" => "A reset password link has been sent to " . $input_array['email'] . ", Please check."
        );
    } else {
        $jsonoutput = array("status" => "error", "msg" => " " . $input_array['email'] . " email is not registered with classified18.com, Please check.");
    }
    // JSON output
    echo jsonoutput(json_encode($jsonoutput));
});

$app->post("/category_list", function ($request, $response) {
    global $conn;
    global $fn; // database and functions
    $data = $request->getParsedBody();
    $input_array = json_decode($request->getBody(), true); // array

    $status_sql = '';
    if ($input_array['status'] != '') {
        $status_sql = 'status= "' . $input_array['status'] . '" ';
    }

    $q = 'SELECT name,permalink,id FROM `og_posts`  WHERE `type`="category" AND ' . $status_sql . ' AND `parentid`="' . $input_array['parentid'] . '" ORDER BY `order` ASC';
    $stmt = $conn->prepare($q);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_OBJ);
    //print_r($result);exit;
    // JSON output
    $jsonoutput = $result;
    echo jsonoutput(json_encode($jsonoutput));
});

$app->post("/product", function ($request, $response) {
    global $conn;
    global $fn; // database and functions
    $data = $request->getParsedBody();
    $input_array = json_decode($request->getBody(), true); // array

    $status_sql = '';
    $category_sql = '';
    $limit_sql = '';

    if ($input_array['q'] != '') {
        $search_sql = ' AND og_posts.name LIKE "%' . $input_array['q'] . '%" ';
    }
    if ($input_array['status'] != '') {
        $status_sql = ' AND status= "' . $input_array['status'] . '" ';
    }
    if ($input_array['category'] != '') {
        $category_sql = ' AND category LIKE "%,' . $input_array['category'] . ',%" ';
    }
    if ($input_array['limit'] != '') {
        $limit_sql = ' LIMIT ' . $input_array['limit'] . ' ';
    }

    if ($input_array['category']) {
        $q = 'select name from `og_posts` where id = ' . $input_array['category'] . '';
        $stmt = $conn->prepare($q);
        $stmt->execute();
        $result_cat = $stmt->fetch(PDO::FETCH_OBJ);
    }
    $where = 'WHERE
og_posts.type="product"  
	' . $search_sql . ' ' . $status_sql . ' ' . $category_sql . '';
    $sql = 'SELECT count(og_posts.id) AS total_product FROM og_posts ' . $where . '';
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $result_total = $stmt->fetch(PDO::FETCH_OBJ);

    $q = 'SELECT
	og_posts.id,
	og_posts.name,
	og_posts.retail_price,
	og_posts.price,
        og_posts.permalink,
	og_post_picture.name AS picture
FROM
	og_posts
	LEFT OUTER JOIN og_post_picture
	 ON og_posts.id = og_post_picture.post_id
' . $where . '
        ORDER BY og_posts.price ASC ' . $limit_sql . ' ';

    $stmt = $conn->prepare($q);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_OBJ);
    //print_r($result);exit;
    // JSON output
    if ($result) {
        //print_r($result);
        $jsonoutput = array("status" => "1", "catname" => $result_cat->name, "total_product" => $result_total->total_product, "data" => $result);
    } else {
        $jsonoutput = array("status" => "0", "catname" => $result_cat->name, "msg" => "Sorry! No product found. ");
    }
    echo jsonoutput(json_encode($jsonoutput));
});

$app->get("/product_details/{id}", function ($request, $response, $args) {
    global $conn; // database and functions
    $proid = $args['id'];
    if ($proid != '') {
        $q = 'SELECT
	og_posts.*,
	og_post_picture.name AS picture
FROM
	og_posts
	LEFT OUTER JOIN og_post_picture
	 ON og_posts.id = og_post_picture.post_id
WHERE
	og_posts.id = ' . $proid . '';
        $stmt = $conn->prepare($q);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_OBJ);
        if ($result) {
            // get post meta
            $sql_meta = 'SELECT	meta_key,meta_value FROM og_postsmeta WHERE post_id = ' . $proid . '';
            $stmt = $conn->prepare($sql_meta);
            $stmt->execute();
            $result_meta = $stmt->fetchAll(PDO::FETCH_OBJ);
            //print_r($result);
            $jsonoutput = array("status" => "success", "data" => $result, "post_meta" => $result_meta);
        } else {
            $jsonoutput = array("status" => "success", "msg" => "Sorry! No product found. ");
        }
    } else {
        $jsonoutput = array("status" => "error,", "msg" => "Product id can't blank, please use user id. ");
    }

    echo jsonoutput(json_encode($jsonoutput));
});

$app->post("/contact_us", function ($request, $response) {
    global $conn;
    global $fn; // database and functions
    $data = $request->getParsedBody();
    $input_array = json_decode($request->getBody(), true); // array
    // JSON output
    $jsonoutput = array("status" => "success", "msg" => "Email has been sent. ", "inputarray" => $input_array);
    echo jsonoutput(json_encode($jsonoutput));
});

$app->post("/search", function ($request, $response) {
    global $conn;
    global $fn; // database and functions
    $data = $request->getParsedBody();
    $input_array = json_decode($request->getBody(), true); // array

    $status_sql = '';
    $category_sql = '';
    $limit_sql = '';
    if ($input_array['status'] != '') {
        $status_sql = ' AND status= "' . $input_array['status'] . '" ';
    }
    if ($input_array['category'] != '') {
        $category_sql = ' AND category LIKE "%,' . $input_array['category'] . ',%" ';
    }
    if ($input_array['q'] != '') {
        $search_sql = ' AND name LIKE "%' . $input_array['q'] . '%" ';
    }
    if ($input_array['limit'] != '') {
        $limit_sql = ' LIMIT ' . $input_array['limit'] . ' ';
    } else {
        $limit_sql = ' LIMIT 0,10 ';
    }

    $q = 'SELECT id,name,permalink,retail_price,price FROM `og_posts`  WHERE `type`="product"  ' . $status_sql . ' ' . $category_sql . ' ' . $search_sql . ' ORDER BY `price` ASC ' . $limit_sql . ' ';
    $stmt = $conn->prepare($q);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_OBJ);
    //print_r($result);exit;
    // JSON output
    $jsonoutput = $result;
    echo jsonoutput(json_encode($jsonoutput));
});

$app->post("/getpageinfo", function ($request, $response) {
    global $conn;
    global $fn; // database and functions
    $data = $request->getParsedBody();
    $input_array = json_decode($request->getBody(), true); // array    
    if ($input_array['outputfield'] != '') {
        $outputfield = $input_array['outputfield'];
    } else {
        $outputfield = '';
    }
    $q = 'SELECT ' . $outputfield . ' FROM `og_posts`  WHERE ' . $input_array['inputfield'] . '="' . $input_array['inputval'] . '" LIMIT 1 ';
    $stmt = $conn->prepare($q);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_OBJ);
    $jsonoutput = $result;
    echo jsonoutput(json_encode($jsonoutput));
});

$app->post("/add2cart", function ($request, $response) {
    global $conn;
    global $fn; // database and functions
    $data = $request->getParsedBody();
    $input_array = json_decode($request->getBody(), true); // array
    // get product details
    $sql = 'SELECT * FROM og_posts WHERE id = "' . $input_array['id'] . '"';
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $prod_data = $stmt->fetch(PDO::FETCH_OBJ);
    if (!$prod_data) {
        $jsonoutput = array("status" => "0", "msg" => "Product not found", "product_id" => $input_array['id']);
        echo jsonoutput(json_encode($jsonoutput));
        exit;
    }
    $new_quantity = 1;
    // check order exits in current session
    $sql = 'SELECT id FROM og_orders WHERE sessionid = "' . $input_array['sessionid'] . '"';
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $result_order = $stmt->fetch(PDO::FETCH_OBJ);
    if ($result_order) {
        //check order details
        $sql_det = 'SELECT id,product_qty FROM og_orders_details WHERE product_id = "' . $input_array['id'] . '" AND orderid = "' . $result_order->id . '" ';
        $stmt = $conn->prepare($sql_det);
        $stmt->execute();
        $result_order_det = $stmt->fetch(PDO::FETCH_OBJ);
        if ($result_order_det) {
            // update quantity
            $new_quantity = $result_order_det->product_qty + 1;
            $input_array_det = array(
                'product_qty' => $new_quantity
            );
            $where = ' id = ' . $result_order_det->id . ' ';
            $fn->update('og_orders_details', $input_array_det, $where);
        } else {
            // add product in order details
            $input_array_det = array(
                'orderid' => $result_order->id,
                'product_id' => $prod_data->id,
                'stock_number' => $prod_data->stock_number,
                'product_name' => $prod_data->name,
                'product_details' => $prod_data->content,
                'product_qty' => $new_quantity,
                'product_price' => $prod_data->price,
                'offer_price' => $prod_data->price
            );
            $fn->insert('og_orders_details', $input_array_det);
        }
    } else {
        // add to order as new order
        $input_array = array(
            'sessionid' => $input_array['sessionid'],
            'date' => $fn->datefull(),
            'status' => 'Draft'
        );
        $insertid = $fn->insert('og_orders', $input_array);
        // add product in order details
        $input_array_det = array(
            'orderid' => $insertid,
            'product_id' => $prod_data->id,
            'stock_number' => $prod_data->stock_number,
            'product_name' => $prod_data->name,
            'product_details' => $prod_data->content,
            'product_qty' => $new_quantity,
            'product_price' => $prod_data->price,
            'offer_price' => $prod_data->price
        );
        $fn->insert('og_orders_details', $input_array_det);
    }
    // JSON output
    $jsonoutput = array("status" => "success", "msg" => "Product has been added to cart", "inputarray" => $prod_data);
    echo jsonoutput(json_encode($jsonoutput));
});

$app->post("/cart_page", function ($request, $response) {
    global $conn;
    global $fn; // database and functions
    $data = $request->getParsedBody();
    $input_array = json_decode($request->getBody(), true); // array
    // order
    $sql = 'SELECT * FROM og_orders WHERE sessionid = "' . $input_array['sessionid'] . '"';
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $result_order = $stmt->fetch(PDO::FETCH_OBJ);
    if ($result_order) {
        // order details
        $sql = 'SELECT *,(offer_price*product_qty) as totalprice FROM og_orders_details WHERE orderid = "' . $result_order->id . '"';
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result_order_det = $stmt->fetchAll(PDO::FETCH_OBJ);

        // JSON output
        $jsonoutput = array("status" => "success", "msg" => "", "order" => $result_order, "order_details" => $result_order_det);
    } else {
        $jsonoutput = array("status" => "0", "msg" => "Cart is empty");
    }
    echo jsonoutput(json_encode($jsonoutput));
});

$app->get("/shipping_method", function ($request, $response) {
    global $conn;
    global $fn; // database and functions
    $data = $request->getParsedBody();
    $input_array = json_decode($request->getBody(), true); // array
    // order
    $sql = 'SELECT id,name,value FROM og_posts WHERE parentid = "42475"';
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_OBJ);
    if ($result) {
        $jsonoutput = array("status" => "success", "msg" => "", "data" => $result);
    } else {
        $jsonoutput = array("status" => "0", "msg" => "Cart is empty");
    }
    echo jsonoutput(json_encode($jsonoutput));
});

$app->post("/update_cart", function ($request, $response) {
    global $conn;
    global $fn; // database and functions
    $data = $request->getParsedBody();
    $input_array = json_decode($request->getBody(), true); // array
    // Update in order details 
    $order_det = $input_array['odid'];
    $order_det_qty = $input_array['qty'];

    foreach ($order_det as $key => $value) {
        $where = ' id = ' . $order_det[$key] . ' ';
        $input_array_det['product_qty'] = $order_det_qty[$key];
        $fn->update('og_orders_details', $input_array_det, $where);
    }

    // get shipping info by id
    $sql = 'SELECT id,value FROM og_posts WHERE id = "' . $input_array['shipping_method'] . '"';
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $result_ship = $stmt->fetch(PDO::FETCH_OBJ);

    // Update in order 
    $where = ' sessionid = "' . $input_array['sessionid'] . '" ';
    $input_array_det = array("shipping_method" => $result_ship->id, "shipping_price" => $result_ship->value);
    $fn->update('og_orders', $input_array_det, $where);

    $jsonoutput = array("status" => "success", "data" => $input_array, "msg" => "");
    echo jsonoutput(json_encode($jsonoutput));
});

$app->post("/remove_product_from_order", function ($request, $response) {
    global $conn;
    global $fn; // database and functions
    $data = $request->getParsedBody();
    $input_array = json_decode($request->getBody(), true); // array
    // remove product from order details table
    if ($input_array['odid'] && $input_array['odid'] != '') {
        $where = ' id = ' . $input_array['odid'] . ' ';
        $fn->delete('og_orders_details', $where);
        $jsonoutput = array("status" => "success", "data" => $input_array, "msg" => "Product has been removed");
    } else {
        $jsonoutput = array("status" => "0", "data" => $input_array, "msg" => "product not found");
    }

    echo jsonoutput(json_encode($jsonoutput));
});

$app->post("/update_shipping", function ($request, $response) {
    global $conn;
    global $fn; // database and functions
    $data = $request->getParsedBody();
    $input_array = json_decode($request->getBody(), true); // array
    // check order based on session id
    $sql = 'SELECT id FROM og_orders WHERE sessionid = "' . $input_array['sessionid'] . '"';
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $result_order = $stmt->fetch(PDO::FETCH_OBJ);
    if ($result_order) {
        // update shipping info
        $orderid = $result_order->id;
        $where = ' id = ' . $orderid . ' ';
        $input_array_data['shipping_info'] = serialize($input_array);
        $fn->update('og_orders', $input_array_data, $where);
        $jsonoutput = array("status" => "1", "msg" => "Shipping information has been updated");
    } else {
        $jsonoutput = array("status" => "0", "data" => $input_array, "msg" => "Order not found");
    }

    echo jsonoutput(json_encode($jsonoutput));
});

$app->post("/cart_sort_info", function ($request, $response) {
    global $conn;
    global $fn; // database and functions
    $data = $request->getParsedBody();
    $input_array = json_decode($request->getBody(), true); // array
    // check order based on session id
    $sql = 'SELECT
	count(og_orders_details.id) AS total_item_cart,
	sum(og_orders_details.product_qty) AS quantity,
	sum(og_orders_details.offer_price * og_orders_details.product_qty) AS total_price
FROM
	og_orders
	INNER JOIN og_orders_details
	 ON og_orders.id = og_orders_details.orderid
WHERE
	og_orders.sessionid = "' . $input_array['sessionid'] . '" ';
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $result_order = $stmt->fetch(PDO::FETCH_OBJ);
    if ($result_order) {
        $jsonoutput = array("status" => "1", "data" => $result_order, "msg" => "Got the cart info");
    } else {
        $jsonoutput = array("status" => "0", "msg" => "Cart is empty");
    }

    echo jsonoutput(json_encode($jsonoutput));
});

$app->post("/track_order", function ($request, $response) {
    global $conn;
    global $fn; // database and functions
    $data = $request->getParsedBody();
    $input_array = json_decode($request->getBody(), true); // array
    // order
    $sql = 'SELECT * FROM og_orders WHERE tracking_id = "' . $input_array['tracking_id'] . '"';
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $result_order = $stmt->fetch(PDO::FETCH_OBJ);
    if ($result_order) {
        // order details
        $sql = 'SELECT *,(offer_price*product_qty) as totalprice FROM og_orders_details WHERE orderid = "' . $result_order->id . '"';
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result_order_det = $stmt->fetchAll(PDO::FETCH_OBJ);
        // grand total
        $sql = 'SELECT sum(offer_price*product_qty) as grandtotal FROM og_orders_details WHERE orderid = "' . $result_order->id . '"';
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result_grand_total = $stmt->fetch(PDO::FETCH_OBJ);

        // JSON output
        $jsonoutput = array("status" => "success", "msg" => "", "order" => $result_order, "order_details" => $result_order_det, "grandtotal" => $result_grand_total);
    } else {
        $jsonoutput = array("status" => "0", "msg" => "Cart is empty");
    }
    echo jsonoutput(json_encode($jsonoutput));
});




//header("contentType","application/json");
$app->contentType("application/json");
$app->run();
?>